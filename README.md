# Slim Skeleton Application

A simple slim skeleton application for rapid development.

### Structure
    ├── composer.json
    ├── config
    │   ├── app
    │   │   └── mvc
    │   │       ├── controllers
    │   │       │   └── Controller.php
    │   │       └── models
    │   │           └── Model.php
    │   ├── config.php
    │   ├── db.php
    │   └── routes
    │       └── index.route.php
    ├── public
    │   └── index.php
    ├── src
    │   ├── controllers
    │   │   └── IndexController.php
    │   ├── models
    │   │   └── EntityModel.php
    │   └── templates
    │       └── index.phtml

*I hope it helps you.*