<?php

// Slim configuration

$dbConfig = require 'db.php';

return [
    'debug' => getenv('APPLICATION_ENV') == 'development' ? true : false,
    'database' => $dbConfig,
    'templates.path' => '../src/templates/',
];
