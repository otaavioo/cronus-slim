<?php

namespace app\mvc\models;

use PDO;

/**
 * Database access layer.
 *
 * @author Diogo Alexsander Cavilha <diogocavilha@gmail.com>
 */
abstract class Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table;

    /**
     * Database object.
     *
     * @var string
     */
    protected $db;

    /**
     * @var Struct\Helpers\Config
     */
    private $config;

    /**
     * Connect to database.
     */
    public function __construct(array $config = array())
    {
        if (!empty($config)) {
            $config = (object) $config;
        } else {
            $app = \Slim\Slim::getInstance();
            $config = (object) $app->config('database');
        }

        $this->db = new PDO(
            "mysql:host={$config->host};dbname={$config->dbname};",
            $config->username,
            $config->password
        );
    }

    /**
     * Insert a data set into a table.
     *
     * @param array $data Associative array to persist on database.
     */
    public function insert(array $data)
    {
        $fields = implode(', ', array_keys($data));
        $values = implode("','", array_values($data));
        $sql = "INSERT INTO `{$this->table}` ({$fields}) VALUES ('{$values}')";

        $this->db->query($sql);
    }

    /**
     * Update a record.
     *
     * @param array  $data  Associative array to persist on database.
     * @param string $where Where clause.
     *
     * @return bool
     */
    public function update(array $data, $where)
    {
        $fields = [];

        foreach ($data as $key => $value) {
            $fields[] = "{$key} = '{$value}'";
        }

        $fields = implode(', ', $fields);
        $sql = "UPDATE {$this->table} SET {$fields} WHERE $where";

        return $this->db->query($sql);
    }

    /**
     * Delete a record from database.
     *
     * @param string $where Where clause.
     *
     * @return bool
     */
    public function delete($where)
    {
        $sql = "DELETE FROM {$this->table} WHERE $where";

        return $this->db->query($sql);
    }

    /**
     * Select all records from table.
     *
     * @param string $where  Where clause
     * @param string $order  Order
     * @param int    $count  From
     * @param int    $offset To
     *
     * @return array Associative array.
     */
    public function fetchAll($where = null, $order = null, $count = null, $offset = null)
    {
        $limit = null;
        $where = (!is_null($where)) ? "WHERE {$where}" : null;
        $order = (!is_null($order)) ? $order : null;

        if (!is_null($count) && !is_null($offset)) {
            $limit = " LIMIT {$offset}, {$count}";
        } elseif (!is_null($count)) {
            $limit = " LIMIT 0, {$count}";
        }

        $sql = "SELECT * FROM {$this->table} {$where} {$order} {$limit}";
        $query = $this->db->query($sql);
        $query->setFetchMode(PDO::FETCH_ASSOC);

        return $query->fetchAll();
    }
}
