<?php

namespace app\mvc\controllers;

abstract class Controller
{
    /**
     * Returns the Slim application instance.
     *
     * @return \Slim\Slim
     */
    protected function getApp()
    {
        return \Slim\Slim::getInstance();
    }

    /**
     * Render a template.
     *
     * @param string $template The name of the template passed into the view's render() method
     * @param array  $data     Associative array of data made available to the view
     * @param int    $status   The HTTP response status code to use (optional)
     */
    protected function render($template, $data = array(), $status = null)
    {
        $this->getApp()->render($template, $data, $status);
    }
}
