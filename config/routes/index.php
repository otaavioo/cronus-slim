<?php

$controller = '\app\controllers\IndexController';

$app->get('/', "$controller:index");
$app->get('/informations', "$controller:collectInformations");
$app->get('/logout', "$controller:logout");
$app->post('/connect', "$controller:connect");
$app->post('/create', "$controller:create");
$app->get('/getTables', "$controller:getTables");
