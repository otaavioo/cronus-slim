<?php

namespace app\models;

use app\mvc\models\Model;
use PDO;

/**
 * Generic model for data managing.
 */
class EntityModel extends Model
{
    /**
     * @var $table
     */
    protected $table = '';

    /**
     * Set the table name.
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * Returns all columns informations from a table.
     * @return
     */
    public function getColumns()
    {
        $query = $this->db->query('SHOW COLUMNS FROM ' . $this->table);
        $query->setFetchMode(PDO::FETCH_ASSOC);

        $fetchColumns = $query->fetchAll();

        $fetchColumns = array_map(
            function ($subArray) {
                $newSubArray = [];
                foreach ($subArray as $key => $value) {
                    $newSubArray[strtolower($key)] = $value;
                }

                return (object) $newSubArray;
            },
            $fetchColumns
        );

        return $fetchColumns;
    }
}
