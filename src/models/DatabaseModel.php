<?php

namespace app\models;

use app\mvc\models\Model;
use PDO;

/**
 * Generic model for data managing.
 */
class DatabaseModel extends Model
{
    /**
     * Fetch all databases from MySQL.
     * @return array Databases list.
     */
    public function fetchAll($where = null, $order = null, $count = null, $offset = null)
    {
        $query = $this->db->query('SHOW DATABASES');
        $query->setFetchMode(PDO::FETCH_OBJ);

        return $query->fetchAll();
    }

    /**
     * Fetch all tables from a database.
     * @return array Tables list.
     */
    public function getTables()
    {
        $query = $this->db->query('SHOW TABLES FROM ' . $_SESSION['database']['dbname']);
        $query->setFetchMode(PDO::FETCH_ASSOC);

        $fetchTables = $query->fetchAll();
        $tables      = [];

        foreach ($fetchTables as $value) {
            $tables[] = reset($value);
        }

        return $tables;
    }
}
