<?php

namespace app\controllers;

use app\mvc\controllers\Controller;
use app\cronus\PhpDoc;
use app\models\EntityModel as Model;
use app\models\DatabaseModel as Database;

class IndexController extends Controller
{
    /**
     * Connection form
     */
    public function index()
    {
        $this->render('index.phtml');
    }

    public function connect()
    {
        $request = $this->getApp()->request;

        if ($request->isPost()) {
            $_SESSION['database'] = [
                'host'     => $request->post('hostname'),
                'username' => $request->post('username'),
                'password' => $request->post('password'),
                'dbname'   => '',
            ];
        }

        $this->getApp()->response->redirect('/informations');
    }

    /**
     * Collects informations about the model which is about to be created
     */
    public function collectInformations()
    {
        $database = new Database($_SESSION['database']);

        $this->render(
            'model-information.phtml',
            [
                'databases' => $database->fetchAll()
            ]
        );
    }

    /**
     * Receives the informations in order to generate the model class code.
     */
    public function create()
    {
        $request = $this->getApp()->request;

        $_SESSION['database']['dbname'] = $request->params('database');

        $model = new Model($_SESSION['database']);
        $model->setTable($request->params('table'));

        $codeGenerator = new PhpDoc($request->params());
        $code          = $codeGenerator->getCode($model->getColumns());

        $this->render('code.phtml', ['code' => $code]);
    }

    public function getTables()
    {
        $request = $this->getApp()->request;

        $_SESSION['database']['dbname'] = $request->params('database');

        $database = new Database($_SESSION['database']);

        echo json_encode($database->getTables());
    }

    public function logout()
    {
        $_SESSION['database'] = null;
        session_destroy();
        $this->getApp()->response->redirect('/');
    }
}
