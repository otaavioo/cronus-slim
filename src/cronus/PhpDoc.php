<?php

namespace app\cronus;

use app\cronus\StringHelper;

/**
 * Manages docblock sutff
 * @author Diogo Alexsander Cavilha <diogocavilha@gmail.com>
 */
class PhpDoc
{
    private $table;
    private $attributeVisibility;
    private $prefix;
    private $className;
    private $tablePrefix;
    private $newLine;
    private $oneTab;
    private $twoTabs;
    private $code;
    private $classe;
    private $typesWithNoLength;

    public function __construct($params)
    {
        $this->table               = $params['table'];
        $this->attributeVisibility = $params['attributeVisibility'];
        $this->prefix              = $params['prefix'];
        $this->className           = trim($params['className']);
        $this->tablePrefix         = isset($params['tablePrefix']) ? $params['tablePrefix'] : '';
        $this->newLine             = "\n";
        $this->oneTab              = "\t";
        $this->twoTabs             = "\t\t";

        $this->typesWithNoLength = [
            'date',
            'datetime',
            'text',
            'float',
            'enum',
        ];
    }

    private function getClassDocBlock()
    {
        $this->code .= '/**' . $this->newLine;
        $this->code .= ' * Class description.' . $this->newLine;
        $this->code .= ' * @copyright copy right here' . $this->newLine;
        $this->code .= ' * @package package name' . $this->newLine;
        $this->code .= ' * @subpackage subpackage name' . $this->newLine;
        $this->code .= ' */' . $this->newLine;
    }

    public function getCode($columns)
    {
        $annotations     = $this->getAttributes($columns);
        $this->className = str_replace($this->tablePrefix, '', $this->table);

        if (!empty($className)) {
            $this->className = $className;
        }

        $this->code .= '<?php' . $this->newLine . $this->newLine;

        $this->getClassDocBlock();

        $this->code .= 'class ' . $this->getClassName() . $this->newLine . '{' . $this->newLine;

        $this->getAnnotations($annotations);
        $this->getGetters($annotations);
        $this->getSetters($annotations);

        return $this->code;
    }

    private function getClassName()
    {
        return ucfirst(StringHelper::toCamelCase($this->className));
    }

    private function getAttributes($columns)
    {
        foreach ($columns as $column) {
            preg_match('/\d+/', $column->type, $output);

            $length         = reset($output);
            $nullable       = (strtolower($column->null) == 'no') ? 'false' : 'true';
            $alias          = $column->field;
            $tempAnnotation = array();

            if (strtolower($column->key) == 'pri') {
                $tempAnnotation['primary']['primary']  = '@Primary';
                $tempAnnotation['primary']['identity'] = '@Identity';
            }

            if (preg_match('/int|tinyint/', $column->type)) {
                $type = 'integer';
            }

            if (preg_match('/varchar|char/', $column->type)) {
                $type = 'string';
            }

            if (preg_match('/text/', $column->type)) {
                $type = 'text';
            }

            if (preg_match('/decimal/', $column->type)) {
                $type = 'float';
                preg_match('/\d+\,\d+/', $column->type, $output);
                $length = reset($output);
                $length = str_replace(',', '.', $length);
            }

            if (preg_match('/^datetime$/', $column->type)) {
                $type = 'datetime';
            }

            if (preg_match('/^date$/', $column->type)) {
                $type = 'date';
            }

            if (preg_match('/float/', $column->type)) {
                $type = 'float';
            }

            if (preg_match('/enum/', $column->type)) {
                $type = 'enum';
                preg_match('/\(.*?\)/', $column->type, $output);
                $tempAnnotation['options'] = reset($output);
            }

            if (preg_match('/^time$/', $column->type)) {
                $type = 'time';
            }

            $tempAnnotation['type']          = $type;
            $tempAnnotation['length']        = $length;
            $tempAnnotation['nullable']      = $nullable;
            $tempAnnotation['alias']         = $alias;
            $tempAnnotation['attributeName'] = str_replace($this->prefix, '', $column->field);
            $annotations[]                   = $tempAnnotation;
        }

        return $annotations;
    }

    private function getAnnotations($annotations)
    {
        foreach ($annotations as $annotation) {
            $this->code .= $this->oneTab . '/**' . $this->newLine;
            $prefixo    = '';

            switch ($annotation['type']) {
                case 'date':
                    $prefixo = 'dta';
                    break;
                case 'datetime':
                    $prefixo = 'dth';
                    break;
                case 'string':
                case 'text':
                    $prefixo = 's';
                    break;
                case 'integer':
                    $prefixo = 'i';
                    break;
                case 'float':
                    $prefixo = 'f';
            }

            if (isset($annotation['primary'])) {
                $this->code .= $this->oneTab . ' * ' . $annotation['primary']['primary'] . $this->newLine;
                $this->code .= $this->oneTab . ' * ' . $annotation['primary']['identity'] . $this->newLine;
            }

            if (in_array($annotation['type'], $this->typesWithNoLength)) {
                if ($annotation['type'] == 'enum') {
                    $annotation['options'] = str_replace('(', '{', $annotation['options']);
                    $annotation['options'] = str_replace(')', '}', $annotation['options']);
                    $this->code .= $this->oneTab . " * @Column(type=\"{$annotation['type']}\", options={$annotation['options']}, nullable={$annotation['nullable']})" . $this->newLine;
                } else {
                    $this->code .= $this->oneTab . " * @Column(type=\"{$annotation['type']}\", nullable={$annotation['nullable']})" . $this->newLine;
                }
            } else {
                $this->code .= $this->oneTab . " * @Column(type=\"{$annotation['type']}\", length={$annotation['length']}, nullable={$annotation['nullable']})" . $this->newLine;
            }

            $this->code .= $this->oneTab . ' */' . $this->newLine;
            $this->code .= $this->oneTab . $this->attributeVisibility . ' $' . StringHelper::toCamelCase($annotation['attributeName']) . ';' . $this->newLine . $this->newLine;
        }

        return $this->code;
    }

    private function getGetters($annotations)
    {
        foreach ($annotations as $atributo) {
            $novoAtributo = StringHelper::toCamelCase($atributo['attributeName']);
            $this->code .= $this->oneTab . 'public function set' . ucfirst(StringHelper::toCamelCase($atributo['attributeName'])) . '($' . StringHelper::toCamelCase($atributo['attributeName']) . ')' . $this->newLine . $this->oneTab . "{" . $this->newLine;
            $this->code .= $this->twoTabs . "\$this->" . StringHelper::toCamelCase($novoAtributo) . ' = $' . StringHelper::toCamelCase($atributo['attributeName']) . ';' . $this->newLine;
            $this->code .= $this->twoTabs . 'return $this;' . $this->newLine;
            $this->code .= $this->oneTab . '}' . $this->newLine . $this->newLine;
        }
    }

    private function getSetters($annotations)
    {
        foreach ($annotations as $atributo) {
            $novoAtributo = StringHelper::toCamelCase($atributo['attributeName']);
            $this->code .= $this->oneTab . 'public function get' . ucfirst(StringHelper::toCamelCase($novoAtributo)) . "()" . $this->newLine . $this->oneTab  . "{" . $this->newLine;
            $this->code .= $this->twoTabs . "return \$this->" . $novoAtributo . ';' . $this->newLine;
            $this->code .= $this->oneTab . '}' . $this->newLine . $this->newLine;
        }

        $this->code .= '}';
    }
}
