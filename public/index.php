<?php

session_cache_limiter(false);
session_destroy();
session_start();

ini_set('highlight.string', "#DD0000");
ini_set('highlight.comment', "#AAAAAA");
ini_set('highlight.keyword', "#007700");
ini_set('highlight.bg', "#FFFFFF");
ini_set('highlight.default', "#0000BB");
ini_set('highlight.html', "#000000");

require '../vendor/autoload.php';

$slimConfig = require '../config/config.php';

$app = new \Slim\Slim($slimConfig);

if ($app->config('debug') === true) {
    ini_set('display_errors', 1);
    error_reporting(-1);
}

// Add all routes
$routes = glob('../config/routes/*.php');
foreach ($routes as $route) {
    require $route;
}

$app->run();
