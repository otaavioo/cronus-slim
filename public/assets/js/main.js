$(document).ready(function() {
    $('#database').change(function() {
        var databaseName = $(this).val();
        $.ajax({
            url: '/getTables',
            type: 'get',
            dataType: 'json',
            data: {
                database: databaseName
            },
            success: function(tables) {
                $('#table').html('');

                $(tables).each(function(i) {
                    $('#table').append($('<option>').attr('value', tables[i]).html(tables[i]));
                });
            }
        });
    });
});
